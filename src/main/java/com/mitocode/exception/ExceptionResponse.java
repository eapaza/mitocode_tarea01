package com.mitocode.exception;

import java.util.Date;
import java.util.List;

public class ExceptionResponse {
	private Date timestamp;
	private String excepcion;
	private List<String> mensaje;
	private String detalles;
	
	public ExceptionResponse(Date timestamp, String excepcion, List<String> mensaje, String detalles) {
		this.timestamp = timestamp;
		this.excepcion = excepcion;
		this.mensaje = mensaje;
		this.detalles = detalles;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public List<String> getMensaje() {
		return mensaje;
	}
	public void setMensaje(List<String> mensaje) {
		this.mensaje = mensaje;
	}
	public String getDetalles() {
		return detalles;
	}
	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	public String getExcepcion() {
		return excepcion;
	}
	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}
	
}
