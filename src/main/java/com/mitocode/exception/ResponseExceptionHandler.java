package com.mitocode.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController

public class ResponseExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> manejarTodasExcepciones(Exception ex, WebRequest request){
		//System.out.println("Entre a exception manejarTodasExcepciones: " + ex.toString());
		List<String> mensaje = new ArrayList<>();
		mensaje.add(ex.getLocalizedMessage());
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getClass().toString(), mensaje, request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/*@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> manejarConstraintViolationException(ConstraintViolationException ex, WebRequest request, Body body){
		String parametros = "";
		System.out.println("Antes de Iterator " + body.toString()); 
		while(request.getParameterNames().hasNext()) {
			System.out.println("Estoy en Iterator " + request.getParameterNames().next()); 
			parametros += request.getParameterNames().next();
		}
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getClass().toString(), parametros, ex.getMessage(), request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
	}*/
	
	@ExceptionHandler(ModeloNotFoundException.class)
	public final ResponseEntity<Object> manejarModeloExcepciones(ModeloNotFoundException ex, WebRequest request){
		List<String> mensaje = new ArrayList<>();
		mensaje.add(ex.getLocalizedMessage());
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getClass().toString(), mensaje, request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	// es necesario adicionar la anotacion @Valid en el controller antes del @RequestBody
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
		List<String> errores = new ArrayList<>();
		//System.out.println("Entre a exception handleMethodArgumentNotValid: " + ex.toString());
		for(ObjectError e: ex.getBindingResult().getAllErrors()) {
			errores.add(e.getDefaultMessage());
		}
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getClass().toString(), errores, request.getDescription(false));
		return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	//HttpMessageNotReadableException
	
}
