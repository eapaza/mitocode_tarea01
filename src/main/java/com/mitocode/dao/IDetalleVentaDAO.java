package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.DetalleVenta;

public interface IDetalleVentaDAO extends JpaRepository<DetalleVenta, Integer>{

}
