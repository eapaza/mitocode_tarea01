package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;

@Controller
@RequestMapping(path = "/ventas")
public class VentaController {
	
	@Autowired
	private IVentaService service;
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Venta> registrar(@Valid @RequestBody Venta venta) {
		Venta ven = service.registrar(venta);
		return new ResponseEntity<Venta>(ven, HttpStatus.CREATED);
	}
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> lstVenta = new ArrayList<Venta>();
		lstVenta = service.listar();
		if (lstVenta.size() == 0) {
			throw new ModeloNotFoundException("No existen datos");
		}
		return new ResponseEntity<List<Venta>>(lstVenta, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Venta> listarPorId(@PathVariable int id){
		Venta venta = service.listarPorId(id);
		if (venta == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		return new ResponseEntity<Venta>(venta, HttpStatus.OK);
	}
	

}
