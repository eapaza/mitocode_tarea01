package com.mitocode.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;


@Controller
@RequestMapping("/productos")
public class ProductoController {
	
	@Autowired
	private IProductoService service;
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Producto> registrar(@Valid @RequestBody Producto producto){
		Producto pro = service.registrar(producto);
		return new ResponseEntity<Producto>(pro, HttpStatus.CREATED);
	}
	
	@PutMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Producto> modificar(@Valid @RequestBody Producto producto){
		// se valida que el id del Producto a modificar exista, si no creara otro registro
		if (service.listarPorId(producto.getIdProducto()) == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		Producto pro = service.modificar(producto);
		return new ResponseEntity<Producto>(pro, HttpStatus.OK);
	}
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> lstPro = service.listar();
		//if (lstPer.isEmpty() == true) {
		if (lstPro.size() == 0) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		return new ResponseEntity<List<Producto>>(lstPro ,HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Producto> listarPorId(@PathVariable("id") int id){
		Producto pro = service.listarPorId(id);
		//if (lstPer.isEmpty() == true) {
		if (pro == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		return new ResponseEntity<Producto>(pro ,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id)
	{
		Producto pro = service.listarPorId(id);
		//if (lstPer.isEmpty() == true) {
		if (pro == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
}
