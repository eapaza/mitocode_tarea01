package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.DetalleVenta;
import com.mitocode.model.Venta;
import com.mitocode.service.IDetalleVentaService;
import com.mitocode.service.IVentaService;

@Controller
//@RequestMapping(path = "/detalleventas")
public class DetalleVentaController {
	
	@Autowired
	private IDetalleVentaService detalleService;

	@Autowired
	private IVentaService ventaService;
	
	/*@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<DetalleVenta> registrar(@Valid @RequestBody DetalleVenta detalleVenta) {
		DetalleVenta detVen = service.registrar(detalleVenta);
		return new ResponseEntity<DetalleVenta>(detVen, HttpStatus.CREATED);
	}*/
	
	@PostMapping(path = "/ventas/{id}/detalleventas", produces = "application/json")
	public ResponseEntity<Venta> registrar(@PathVariable(value = "id") int id,
										   @Valid @RequestBody DetalleVenta detalleVenta){
		//System.out.println("Entre al API 4");
		Venta venta = ventaService.listarPorId(id);
		if (venta == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		
		detalleVenta.setVenta(venta);
		detalleService.registrar(detalleVenta);
		Venta ventaNue = ventaService.listarPorId(id);
		return new ResponseEntity<Venta>(ventaNue, HttpStatus.CREATED);
	}
	
	@GetMapping(path = "/detalleventas", produces = "application/json")
	public ResponseEntity<List<DetalleVenta>> listar(){
		List<DetalleVenta> lstDetVenta = new ArrayList<DetalleVenta>();
		lstDetVenta = detalleService.listar();
		if (lstDetVenta.size() == 0) {
			throw new ModeloNotFoundException("No existen datos");
		}
		return new ResponseEntity<List<DetalleVenta>>(lstDetVenta, HttpStatus.OK);
	}
	
	@GetMapping(path = "/detalleventas/{id}", produces = "application/json")
	public ResponseEntity<DetalleVenta> listarPorId(@PathVariable (value = "id") int id){
		DetalleVenta detVenta = detalleService.listarPorId(id);
		if (detVenta == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		return new ResponseEntity<DetalleVenta>(detVenta, HttpStatus.OK);
	}
	

}
