package com.mitocode.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;


@Controller
@RequestMapping("/personas")
public class PersonaController {
	
	@Autowired
	private IPersonaService service;
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Persona> registrar(@Valid @RequestBody Persona persona){
		Persona per = service.registrar(persona);
		return new ResponseEntity<Persona>(per, HttpStatus.CREATED);
	}
	
	@PutMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona persona){
		// se valida que el id de la persona a modificar exista, si no creara otro registro
		if (service.listarPorId(persona.getIdPersona()) == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		Persona per = service.modificar(persona);
		return new ResponseEntity<Persona>(per, HttpStatus.OK);
	}
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Persona>> listar(){
		List<Persona> lstPer = service.listar();
		//if (lstPer.isEmpty() == true) {
		if (lstPer.size() == 0) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		return new ResponseEntity<List<Persona>>(lstPer ,HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Persona> listarPorId(@PathVariable("id") int id){
		Persona per = service.listarPorId(id);
		//if (lstPer.isEmpty() == true) {
		if (per == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		return new ResponseEntity<Persona>(per ,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id)
	{
		Persona per = service.listarPorId(id);
		//if (lstPer.isEmpty() == true) {
		if (per == null) {
			throw new ModeloNotFoundException("No se encontraron datos");
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
}