package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IDetalleVentaDAO;
import com.mitocode.model.DetalleVenta;
import com.mitocode.service.IDetalleVentaService;

@Service
public class DetalleVentaServiceImpl implements IDetalleVentaService {
	
	@Autowired
	private IDetalleVentaDAO dao; 
	
	@Override
	public DetalleVenta registrar(DetalleVenta t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}

	@Override
	public List<DetalleVenta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public DetalleVenta listarPorId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}
}
