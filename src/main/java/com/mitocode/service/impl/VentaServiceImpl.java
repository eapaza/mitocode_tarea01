package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dao.IVentaDAO;
import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{
	
	@Autowired
	private IVentaDAO dao; 
	
	@Transactional
	@Override
	public Venta registrar(Venta t) {
		// TODO Auto-generated method stub
		
		t.getDetalleVenta().forEach(d->{
			d.setVenta(t);
		});
		
		return dao.save(t);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Venta listarPorId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}
}
