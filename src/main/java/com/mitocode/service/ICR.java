package com.mitocode.service;

import java.util.List;

public interface ICR<T> {
	T registrar(T t);
	List<T> listar();
	T listarPorId(int id);
}
